﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraMovement : MonoBehaviour
{
    [SerializeField]
    float smoothness;
    [SerializeField]
    Transform focusTarget;
    Vector3 relativePosition;
    Vector3 desiredPosition;

    [SerializeField]
    int maxInterpolations;

    void Awake()
    {
        relativePosition = transform.position - focusTarget.position;
    }

    void FixedUpdate()
    {
        // Check what camera position is best
        List<Vector3> camPositions = CalculatePositions();
        foreach(Vector3 p in camPositions)
        {
            if(CheckLineOfSight (p))
            {
                desiredPosition = p;
                break;
            }
        }

        // Smoothly translate camera over to desired position
        transform.position = Vector3.Lerp (transform.position, desiredPosition, Time.deltaTime * smoothness);

        // Smoothly rotate camera to look at target
        Vector3 camToTarget = focusTarget.position - transform.position;
        Quaternion lookAtRotation = Quaternion.LookRotation(camToTarget, Vector3.up);
        transform.rotation = Quaternion.Lerp (transform.rotation, lookAtRotation, smoothness * Time.deltaTime);
    }

    List<Vector3> CalculatePositions()
    {
        Vector3 abovePosition = focusTarget.position + Vector3.up * relativePosition.magnitude;
        Vector3 standardPosition = focusTarget.position + relativePosition;
        
        List<Vector3> camPositions = new List<Vector3>();
        camPositions.Add (standardPosition);
        for(int i = 0; i < maxInterpolations; i++)
        {
            float t = (float)i / maxInterpolations;
            camPositions.Add (Vector3.Lerp (standardPosition, abovePosition, t));
        }
        camPositions.Add (abovePosition);

        return camPositions;
    }

    bool CheckLineOfSight(Vector3 position)
    {
        RaycastHit hit;
        if(Physics.Raycast(position, focusTarget.position - position, out hit, relativePosition.magnitude))
        {
            if(hit.transform != focusTarget)
            {
                return false;
            }
        }
        return true;
    }

    public void FocusTarget(Transform newTarget)
    {
        focusTarget = newTarget;
    }

    public Transform GetTarget()
    {
        return focusTarget;
    }
}
